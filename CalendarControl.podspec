Pod::Spec.new do |s|
  s.name         = "CalendarControl"
  s.version      = "0.0.2"
  s.summary      = "Custom UI for selecting dates on calendar."
  s.homepage     = "https://gitlab.com/faruuqq/calendarcontrol.git"
  # s.license      = "MIT"
  s.license    = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Faruuq" => "faruuqq@icloud.com" }
  s.platform     = :ios, "13.0"
  s.source       = { :git => "https://gitlab.com/faruuqq/calendarcontrol.git", :tag => "#{s.version}" }
  # s.source_files  = "Classes", "Classes/**/*.{h,m}"
  # s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"
  # s.framework  = "CalendarControl"
  # s.vendored_frameworks = "xcframeworks/CalendarControl.xcframework"
  
s.subspec "Core" do |ss|
    ss.source_files  = "CalendarControl/**/*"
    ss.dependency "Alamofire", "~> 5.0"
    ss.framework  = "Foundation"
end
end
